from ._main import DIRS2, Embed, STAT_TYPES
import tabulate, collections

tables=['Weapon','Armor','Helmet','Accessories']

def Enchant(page):
	if page in tables:
		tdata=[
			[
				*Transform(key,vals)
			]
			for key,vals in etypes[tables.index(page)].items()
		]
		headers=('Stat','Range 1', 'Range 2', 'Range 3', 'Range 4')
	elif page == 'Chances':
		tdata=[
			[1 ,'80.0%', '20.0%', '0.0%'],
			[2 ,'17.0%', '68.0%', '35.0%'],
			[3 ,'3.0%', '10.0%', '60.0%'],
			[4 ,'0.0%', '2.0%', '5.0%'],
		]
		headers=('Range', 'R', 'SR', 'SSR')
	
	return '**%s**\n%s'%(page.title(),'```%s```'%tabulate.tabulate(tdata,headers))

def Transform(key,vals):
	mod=key[-3:]
	key=STAT_TYPES[key[25:-3]]

	if mod == 'Mul' or key[:3] == 'Cri':
		return (key,*['%s%% - %s%%'%(int(vr[0]/100),int(vr[1]/100)) for vr, chances in vals.items()])
	elif mod == 'Add':
		return (key,*['%s - %s'%vr for vr, chances in vals.items()])
	else:
		print(mod,key)

####	create tables
data = DIRS2['EnchantTemplate']
etypes = [[data[i+j*4] for j in range(3)] for i in range(1,5)]

pi='Property%sID'
vr='ValueRange%s'
we='Weight%s'

for e,etyp in enumerate(etypes):
	vals={}
	#collect all value ranges for a typ
	for r,rarity in enumerate(etyp):
		i=1
		while we%i in rarity:
			if rarity[we%i] == 0:
				i+=1
				continue
			prop = rarity[pi%i]
			if prop not in vals:
				vals[prop] = {}
			for item in rarity[vr%i]:
				vr_key = (item['Min'],item['Max'])
				if vr_key not in vals[prop]:
					vals[prop][vr_key]=[0]*3
				vals[prop][vr_key][r]=item['Weight']
			i+=1

	for k,val in vals.items():
		vals[k]=collections.OrderedDict(sorted(list(val.items()), key=lambda t:int(t[0][0])))
	etypes[e]=collections.OrderedDict(sorted(list(vals.items()), key=lambda t:t[0][25:]))